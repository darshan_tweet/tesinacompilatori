# Project: Tesina

OBJ      = lex.yy.o parser.tab.o strutture.o
LINKOBJ  = lex.yy.o parser.tab.o strutture.o
BIN      = Tesina.bin

.PHONY: all all-before all-after clean clean-custom

all: all-before $(BIN) all-after

clean: clean-custom
	${RM} $(OBJ) $(BIN)

$(BIN): $(OBJ)
	$(CC) $(LINKOBJ) -o $(BIN) $(LIBS)

lex.yy.o: lex.yy.c
	$(CC) -c lex.yy.c -o lex.yy.o $(CFLAGS)

parser.tab.o: parser.tab.c
	$(CC) -c parser.tab.c -o parser.tab.o $(CFLAGS)

strutture.o: strutture.c
	$(CC) -c strutture.c -o strutture.o $(CFLAGS)
