#include "strutture.h"

void elemento_dump(Lista *l) {
	if (l == NULL ) {
		printf("NULL\n");
	} else {
		printf("%s:{ %s ; %s ; %d }  [%p <- %p -> %p] \n", l->codice, l->titolo,
				l->autore, l->richieste , l->prev, l, l->next);
	}
}

void lista_dump(Lista *testa) {
	Lista *l = (Lista*) testa->next;
	while (l != NULL ) {
		elemento_dump(l);
		l = (Lista*) l->next;
	}
}

int hash(char* etichetta) {
	int n = 0, key = 0;
	static const int base=127;
	while (etichetta[n] != 0) {
		key = (base*key + etichetta[n]) % DIMENSIONE;
		n++;
	}
	return key;
}

/**
 * Accoda l'elemento nella lista di trabocco corrispondente all'hash
 */
void hash_insert(Lista *dato, Lista vettore[]) {
	if(dato==NULL || vettore==NULL){
		puts("Parametri non validi");
		return;
	}
	int k = hash(dato->codice);
	Lista *temp = &(vettore[k]);
	if(temp==NULL)return;
	while (temp->next != NULL )
		temp = temp->next;
	temp->next = dato;
	dato->prev = temp;
}

Lista* hash_get(char* codice, Lista vettore[]) {
	int k = hash(codice);
	Lista *temp = vettore[k].next; //la testa non si tocca
	if(temp==NULL){
		return NULL;
	};
	while (strcmp(temp->codice, codice) != 0) {
		if (temp->next == NULL )
			return NULL ;
		temp = temp->next;
	}
	return temp;
}
;

int hash_remove(char* codice, Lista vettore[]) {
	Lista *dato = hash_get(codice, vettore);
	Lista *tmp;
	if (dato == NULL )
		return -1;
	if (dato->prev == NULL && dato->next == NULL ) {
		printf("Questo non dovrebbe assolutamente succedere");
		return -1;
	};
	if (dato->prev != NULL ) {
		tmp = dato->prev;
		tmp->next = dato->next;
	};
	if (dato->next != NULL ) {
		tmp = dato->next;
		tmp->prev = dato->prev;
	};
	free(dato->codice);
	free(dato->titolo);
	free(dato->autore);
	free(dato);
	return 1;
}
;

Lista* hash_init() {
	Lista *l;
	int i;
	l = (Lista*) malloc(DIMENSIONE * sizeof(Lista));
	for (i = 0; i < DIMENSIONE; i++) {
		l[i].autore = NULL;
		l[i].codice = NULL;
		l[i].titolo = NULL;
		l[i].richieste=0;
		l[i].next = NULL;
		l[i].prev = NULL;
	}
	return l;
}

void hash_dump(Lista *l) {
	int i;
	for (i = 0; i < DIMENSIONE; i++) {
		printf("[%d]\n", i);
		lista_dump(&(l[i]));
	}
}

int hash_free(Lista *l) {
	int i;
	int n = 0;
	Lista *tmp;
	for (i = 0; i < DIMENSIONE; i++) {
		tmp = l[i].next;//salta la testa.
		while (tmp->next != NULL ) {
			tmp = tmp->next;
			free(tmp->prev->codice);
			free(tmp->prev->titolo);
			free(tmp->prev->autore);
			free(tmp->prev);
			n++;
		}
		free(tmp->codice);
		free(tmp->titolo);
		free(tmp->autore);
		free(tmp);
		l[i].next = NULL;
	}
	return n;
}

/**
 * modo crossplatform per aspettare la pressione di un tasto
 */
int pause(){
    int i;

    for (i = 0; i < 4; i++) {
        printf("Premi INVIO per continuare: ... ");
        if( getchar() == '\n')return 0;
    }    
    return 1;
}
