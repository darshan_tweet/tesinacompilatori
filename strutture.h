#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

#ifndef STRUTTURE_H
#define STRUTTURE_H

typedef struct _lista{
    char *codice;
    char *titolo;
    char *autore;
    int richieste;
    struct _lista *prev;
    struct _lista *next;
}Lista;

int linee;

//
#define DIMENSIONE 3 // per fare test di carico 
#define TRUE 1
#define FALSE 0
//#define DIMENSIONE 193 // per la release, un numero primo.

void elemento_dump(Lista *l);
void lista_dump(Lista *testa);
void hash_dump(Lista *l);
int hash(char* etichetta);
void hash_insert(Lista *dato, Lista vettore[]);
Lista* hash_get(char* codice, Lista vettore[]);
int hash_remove(char* codice, Lista vettore[]);
Lista* hash_init();
int hash_free(Lista *l);
int pause();
#endif
