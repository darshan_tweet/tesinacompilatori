%{
	#include "strutture.h"
	#include <errno.h>
   	void yyerror (const char *s);
   	extern FILE * yyin;
   	Lista *tabella;
   	Lista *e;
   	int completo=TRUE;

%}

%union {
 int num;
 char *str;
}


%token SEPARATORE;
%token DELIMITATORESETTORE;
%token DELIMITATORERECORD;
%token DELIMITATOREOPERAZIONE

%token <str> CODICE;
%token <str> TITOLO;
%token <str> AUTORE;
%token <str> NOME;
%token <num> NUMERO;


%locations
%start Assioma
%error-verbose
%%

Assioma: Elenco_libri DELIMITATORESETTORE Elenco_operazioni
	| Elenco_libri DELIMITATORESETTORE
	{printf("Il file non contiene operazioni");completo=FALSE;}
	| Elenco_libri 
	{printf("Il file non contiene operazioni");completo=FALSE;}
;

Elenco_libri: Elenco_libri DELIMITATORERECORD Libro 
	     | Libro 
;

Libro: CODICE TITOLO AUTORE
      {
		e = (Lista*) malloc(sizeof(Lista));		
		e->codice = $1;
		e->titolo = $2;
		e->autore = $3;
		e->richieste=0;
		e->prev=NULL;
		e->next=NULL;
		hash_insert(e, tabella);
    }
;

Elenco_operazioni: Elenco_operazioni Operazione 
	     | Operazione
;

Operazione: CODICE SEPARATORE NUMERO NOME DELIMITATOREOPERAZIONE
	    {richiesta($1,$3,$4,tabella); free($1); free($4);}
;	    
%%

int richiesta(char* codice, int numero,char* nome, Lista* vettore){
  Lista *elemento=hash_get(codice, vettore);
  if(elemento==NULL){
    yyerror("Il testo non e' presente in archivio (errore semantico)");
    return FALSE;
  };
  elemento->richieste+=numero;
  printf("Richiesta registrata\n");
  return TRUE;  
}

Lista* piuvenduto(Lista* vettore){
  int record=0,i;
  Lista *e;
  Lista *iteratore;
  for (i = 0; i < DIMENSIONE; i++) {
	  iteratore=&vettore[i];
	  if(iteratore==NULL)return NULL;
	  iteratore=iteratore->next;
	  while (iteratore != NULL ) {
		  if(iteratore->richieste > record){
		    record=iteratore->richieste;
		    e=iteratore;
		  }
		  iteratore = (Lista*) iteratore->next;
	  }
  }  
  return e;
}

void decalloca(){
  hash_free(tabella);
}


int main(int argc, char *argv[])
{
	int ris;
	char output[256];
	FILE *foutput;
	puts(argv[0]);
	if(argc<2){
	  printf("Non e' stato specificato nessun file in input, verrà usato lo stdin\n");
	}else{
	  strcpy(output,argv[1]);	  
	  strcat(output, "_out.txt");
	  printf("Userò <%s> come file di input e <%s> come file di output\n",argv[1],output);
	  yyin=fopen(argv[1], "r");
	  if(yyin==NULL){
	    printf("Impossibile accedere al file di input\n (%s)\n",strerror(errno));
	    return FALSE;
	  }
	}
	tabella=hash_init();
	ris=yyparse();
	printf("\nStampo un dump dell'hashmap\n");
	puts("-----------------------------------");
	hash_dump(tabella);
	puts("-----------------------------------");
	if(ris!=0){
	  printf("Si sono verificati degli errori,il calcolo delle copie non puo' essere effettuato.");
	  return FALSE;
	}
	if(!completo){
	  printf("Il file non conteneva richieste, non c'e' niente da contare\n");
	  return TRUE;
	}
	printf("Caricamento completato\n Produco il file di output col nome del libro piu' richiesto.\n");
	Lista *e=piuvenduto(tabella);
	elemento_dump(e);
	if(argc<2){
	    foutput=stdout;
	  }else{
	    foutput=fopen(output,"w+");
	  }
	if(foutput==NULL){
	  printf("Impossibile accedere al file di output\n (%s)\n",strerror(errno));
	  return FALSE;
	}else{
	  fprintf(foutput,"%s\n%d\n",e->titolo,e->richieste);
	  fclose(foutput);
	  printf("Scrittura completata correttamente.\n");
	}	
	decalloca();
	pause();
	return TRUE;
}


void yyerror (const char *s){
	printf("\nErrore di sintassi:\n\t %s \n\tAlla linea %d \n", s, linee);
}
