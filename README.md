# Tesina compilatori
Il progetto consiste nell'analisi di un particolare linguaggio che permetta di costruire un archivio di testi e di richieste e poi di estrarre il nome del testo più richiesto corredato dal numero di richieste.

L'eseguibile prodotto dovrà quindi:

1.	Verificare la correttezza sintattica dell'input
2.	Verificare la correttezza semantica evitando di eseguire operazioni non permesse sul set di dati (richieste di testi non esistenti per esempio)
3.	In caso di errori semantici provare comunque ad eseguire il task.
4.	Archiviare i testi in una struttura a dizionario (in particolare una hashmap)
5.	Produrre in output il nome e il numero di richieste del libro più richiesto.

## Verifica della correttezza sintattica
Per eseguire l'analisi lessicale si è innanzi tutto definito un set di pattern che rappresentano i token con attributi da passare all'analizzatore sintattico:

numero
:	rappresenta un numero, la sua espressione regolare è quindi [0-9]+ e il suo attributo sarà un numero

codice
:	rappresenta un codice alfanumerico costituito da lettere maiuscole, viene utilizzato per i codici dei libri sia in fase di archiviazione che in fase di operazione. la sua espressione regolare è [A-Z0-9]+ e il suo attributo sarà una stringa

autore
:	rappresenta un autore come una stringa costituita da parole con iniziali maiuscole e corpo minuscolo, la sua espressione regolare è ([A-Z][a-z]+" "*)+[a-z] e il suo attributo sarà una stringa.

titolo
:	rappresenta il titolo di un libro o il nome di una libreria come una stringa composta da parole di sole lettere maiuscole, la sua espressione regolare è ([A-Z]+" "*)+[A-Z] e il suo attributo sarà una stringa.

La struttura dei token vista dal Parser conterrà quindi il nome del token e un attributo che può essere intero o stringa ( definito come una union ).

    %union {
    int num;
    char *str;
    }

Le stringhe passate al Parser saranno quindi copie di quelle lette dal Lexer e andranno gestite e deallocate dal Parser.
    
Si è cercato di limitare il numero di token passati all'analizzatore sintattico in modo da snellire la grammatica, in particolare nelle definizioni dei testi

```Yaml
Codice: CODICE
Titolo: TITOLO
Autore: AUTORE
```

Le stringe "Codice:","Titolo:","Autore:" non producono token e alla grammatica viene passata una struttura del tipo

    CODICE TITOLO AUTORE

che permette di definire un libro con una sola produzione.
Per fare questo si sono usate 3 diverse start condition, le strutture delle 3 sono molto simili e riporto qui solo la prima:

```C
^"Codice: "	{BEGIN(Cod);fprintf(yyout, "Analizzo un codice\n");}
<Cod>{codice}	{
		      fprintf(yyout, "	->");
		      ECHO;
		      BEGIN(0);
		      yylval.str=strdup(yytext);
		      return CODICE; 
		      }
<Cod>[^A-Z0-9]	{
		      BEGIN(0);
		      fprintf(yyout, "Codice non corretto, la riga verrà ignorata\n");
		      ECHO;
		      }
```

La start condition viene iniziata dalla stringa "Codice: " e terminata in ogni caso dopo il match con un lessema. 
Il Lexer riconosce solo le singole righe che non hanno un formato corretto ( dopo "Codice: " ci deve essere un codice, dopo "Titolo: " un titolo, dopo "Autore: " un autore).
Se il Lexer non riconosce una riga la scarta provocando al livello superiore un errore di sintassi rilevato dal Parser che non riceve una struttura del tipo "CODICE TITOLO AUTORE".

Per quanto riguarda le richieste si è deciso di produrre token da tutti i lessemi

    CODICE –> NUMERO NOME ;

vengono passate al Parser come

    CODICE SEPARATORE NUMERO NOME DELIMITATOREOPERAZIONE

Potendo comunque rappresentare la struttura con una sola produzione.

Gestendo i token in questo modo la grammatica diventa molto semplice:
\pagebreak

---------------

```Yaml
Assioma: Elenco_libri DELIMITATORESETTORE Elenco_operazioni
	| Elenco_libri DELIMITATORESETTORE
	| Elenco_libri 
;

Elenco_libri: Elenco_libri DELIMITATORERECORD Libro 
	     | Libro 
;

Libro: CODICE TITOLO AUTORE
;

Elenco_operazioni: Elenco_operazioni Operazione 
	     | Operazione
;

Operazione: CODICE SEPARATORE NUMERO NOME DELIMITATOREOPERAZIONE
;	    
```

---------------

L'analizzatore ricerca l'assioma che è definito con 3 produzioni non ricorsive che rappresentano i 3 modi in cui si può presentare il file di input:

1.	Completo sia di un elenco di libri che di operazioni
2.	Con solo un elenco di libri ma con il separatore delle sezioni
3.	Con solo un elenco di libri senza separatore delle sezioni

I non terminali "Operazione" e "Libro" sono definiti da una singola produzione.
I non terminali "Elenco_libri" ed "Elenco_operazioni" sono definiti per ricorsione sinistra utilizzando un singolo Libro o una Singola operazione come produzione base.

Essendo Bison implementato attraverso un Parser bottom-up (in particolare ,salvo altrimenti specificato, con un "LALR(1)" ) la ricorsione sinistra è da preferire alla ricorsione destra. Nei Parser bottom-up la procedura di parsing è vista come la progressiva riduzione delle stringhe ai simboli che rappresentano.
A ogni passo della riduzione una sottostringa viene sostituita alla testa della produzione che la rappresenta, la ricorsione destra necessiterebbe di avere tutti i simboli nello stack prima di poter effettuare i "reduce".

## Analisi semantica ed esecuzione del task
L'esecuzione del task si svolge in due fasi: nella prima vengono caricati i dati e nella seconda si cerca all'interno del dizionario il libro con più copie ordinate.

Il caricamento dei dati avviene in fase di parsing, ogni volta che viene individuato un libro lo si aggiunge al dizionario e ogni volta che viene individuata una richiesta si incrementa il numero di richieste per il libro corrispondente all'interno del dizionario.
Queste due operazioni vengono quindi eseguite contestualmente al parsing dei non terminali "Libro" e "Operazione"

### Aggiunta di un libro nel dizionario

```C
Libro: CODICE TITOLO AUTORE
      {
		e = (Lista*) malloc(sizeof(Lista));		
		e->codice = $1;
		e->titolo = $2;
		e->autore = $3;
		e->richieste=0;
		e->prev=NULL;
		e->next=NULL;
		hash_insert(e, tabella);
    }
;
```

### Parsing degli ordini (e analisi semantica)

```Yaml
Operazione: CODICE SEPARATORE NUMERO NOME DELIMITATOREOPERAZIONE
	    {richiesta($1,$3,$4,tabella); free($1); free($4);}
;
```
Dove 

    int richiesta(char* codice, int numero,char* nome, Lista* vettore) 
è definito come:

```C
int richiesta(char* codice, int numero,char* nome, Lista* vettore)
{
  Lista *elemento=hash_get(codice, vettore);
  if(elemento==NULL){
    yyerror("Il testo non e' presente in archivio (errore semantico)");
    return FALSE;
  };
  elemento->richieste+=numero;
  printf("Richiesta registrata\n");
  return TRUE;  
}
```
La funzione controlla se l'elemento è effettivamente presente nella tabella dei simboli generando un errore semantico in caso contrario.
Nel caso di errori semantici semplicemente l'ordine viene ignorato e si procede col parsing degli ordini successivi.
Le chiamate a free() sono necessarie in quanto gli attributi stringa sono ottenuti duplicando le stringhe del lessema, si tratta quindi di allocazione dinamica che va gestita.

### Ricerca del libro più venduto
Finita la fase di parsing viene cercato il libro più venduto scorrendo l'intero archivio

```C
Lista* piuvenduto(Lista* vettore){
  int record=0,i;
  Lista *e;
  Lista *iteratore;
  for (i = 0; i < DIMENSIONE; i++) {
	  iteratore=&vettore[i];
	  if(iteratore==NULL)return NULL;
	  iteratore=iteratore->next;
	  while (iteratore != NULL ) {
		  if(iteratore->richieste > record){
		    record=iteratore->richieste;
		    e=iteratore;
		  }
		  iteratore = (Lista*) iteratore->next;
	  }
  }  
  return e;
}
```

Se non si sono verificati errori nelle fasi precedenti la funzione può sicuramente essere eseguita con successo.

## Struttura del main
L'entry point del progetto è definita nel file del Parser (parser.y), il programma prende i dati da un file in input e produce un file in output con il nome e il numero di copie richieste del libro più richiesto.
Nel caso non fosse specificato nessun file in input si può procedere in modo interattivo ma non verrà prodotto alcun file in output.
Vengono stampate informazioni dettagliate sull'esecuzione nello "standard output".

-----------------------

```C
int main(int argc, char *argv[])
{
	int ris;
	char output[256];
	FILE *foutput;
	puts(argv[0]);
	if(argc<2){
	  printf("Non e' stato specificato nessun file in input, verrà usato lo stdin\n");
	}else{
	  strcpy(output,argv[1]);	  
	  strcat(output, "_out.txt");
	  printf("Userò <%s> come file di input e <%s> come file di output\n",argv[1],output);
	  yyin=fopen(argv[1], "r");
	  if(yyin==NULL){
	    printf("Impossibile accedere al file di input\n (%s)\n",strerror(errno));
	    return FALSE;
	  }
	}
	tabella=hash_init();
	ris=yyparse();
	printf("\nStampo un dump dell'hashmap\n");
	puts("-----------------------------------");
	hash_dump(tabella);
	puts("-----------------------------------");
	if(ris!=0){
	  printf("Si sono verificati degli errori,il calcolo delle copie non puo' "
		  "essere effettuato.");
	  return FALSE;
	}
	if(!completo){
	  printf("Il file non conteneva richieste, non c'e' niente da contare\n");
	  return TRUE;
	}
	printf("Caricamento completato\n Produco il file di output col nome del libro "
		"piu' richiesto.\n");
	Lista *e=piuvenduto(tabella);
	elemento_dump(e);
	if(argc<2){
	    foutput=stdout;
	  }else{
	    foutput=fopen(output,"w+");
	  }
	if(foutput==NULL){
	  printf("Impossibile accedere al file di output\n (%s)\n",strerror(errno));
	  return FALSE;
	}else{
	  fprintf(foutput,"%s\n%d\n",e->titolo,e->richieste);
	  fclose(foutput);
	  printf("Scrittura completata correttamente.\n");
	}	
	decalloca();
	pause();
	return TRUE;
}
```

-----------------------

## Struttura dati utilizzata per la tabella dei simboli
Si è deciso di utilizzare per la tabella dei simboli una hash map con liste di trabocco (hash table with chaining).
In questa implementazione ad ogni simbolo da memorizzare viene associato un hash che rappresenta la locazione del vettore in cui verrà salvato.
L'elemento viene quindi accodato alla lista presente in quella locazione dell'array.

Le liste hanno un record di header che non viene modificato.
In questo modo ci si assicura che il vettore che rappresenta la hashmap contenga sempre puntatori all'inizio della lista anche se viene eliminato il primo record.
Il vettore ha lunghezza "DIMENSIONE" che è una costante definita attraverso una macro in strutture.h
Al fine di facilitare il testing e di provocare molte collisioni la dimensione è stata scelta molto bassa.
In fase di produzione è invece conveniente scegliere un numero primo sufficientemente grande da provocare un basso numero di collisioni e possibilmente non troppo vicino a una potenza di 2.

La funzione di hash viene scelta in modo da provocare una distribuzione uniforme delle chiavi sugli indici, ogni chiave ha la stessa probabilità di essere mappata in una delle locazioni del vettore, indipendentemente dalla locazione cui è mappata ogni altra chiave.


Se N è il numero variabile degli elementi e M è la dimensione fissata del vettore, si definisce fattore di
carico 
$$ \alpha =N/M $$
Partendo dal presupposto di una distribuzione uniforme i tempi di accesso medi possono essere stimati in base al fattore di carico.
In particolare il tempo medio della scansione ( e quindi dell'inserimento o di una ricerca di un elemento non esistente) è
$$ \Theta (1+\alpha) $$
Mentre il tempo medio per ottenere una chiave presente è
$$ \Theta (1+ \frac{ \alpha }{ 2 } ) $$
(semplicemente perché il fattore di carico è proporzionale alla lunghezza media delle liste di trabocco e mediamente un elemento si troverà a metà di una lista)

La funzione di hash deve lavorare su stringhe ed è quindi definita come:

```C
int hash(char* etichetta) {
	int n = 0, key = 0;
	static const int base=127;
	while (etichetta[n] != 0) {
		key = (base*key + etichetta[n]) % DIMENSIONE;
		n++;
	}
	return key;
}
```
Questo corrisponde al trattare la stringa come un grande numero binario e definire la chiave come il resto della divisione del numero per la dimensione dell'hash, il codice esegue in fatti il calcolo di un polinomio con la regola di Horner.

$$ a_n \cdot x^n + a_{n-1} \cdot x^{n-1} + \ldots + a_1 \cdot x + a_0 \Longleftrightarrow  x( \ldots (x(a^n \cdot x) + a_{n-1}) \ldots ) + a_1 ) + a_0 $$

In questo modo ogni carattere contribuisce in modo unico al calcolo della Key provocando la distribuzione uniforme che si cercava.

(se le etichette fossero particolarmente grandi sarebbe opportuno generare l'hash da un sottoinsieme dei caratteri, ma non è questo il caso)


le funzioni implementate nell'hashmap sono le seguenti:

```C
void hash_dump(Lista *l);
int hash(char* etichetta);
void hash_insert(Lista *dato, Lista vettore[]);
Lista* hash_get(char* codice, Lista vettore[]);
int hash_remove(char* codice, Lista vettore[]);
Lista* hash_init();
int hash_free(Lista *l);
```

Lista è la struttura dati che definisce il record utilizzato per i testi

```C
typedef struct _lista{
    char *codice;
    char *titolo;
    char *autore;
    int richieste;
    struct _lista *prev;
    struct _lista *next;
}Lista;
```
    
## Sorgenti completi

### Sorgente per Flex

---------------


```C
    #include <stdio.h>
    #include <string.h>
    #include <ctype.h>
    #include "strutture.h"
    #include "parser.tab.h"
%option noyywrap
%option yylineno

%x Cod Tit Aut Ope
numero [0-9]+
codice [A-Z0-9]+
autore ([A-Z][a-z]+" "*)+[a-z]
titolo ([A-Z]+" "*)+[A-Z]

%%
^"$$"	{ECHO; return DELIMITATORESETTORE; }
^"##"	{ECHO; return DELIMITATORERECORD ; }

^"Codice: "	{BEGIN(Cod);fprintf(yyout, "Analizzo un codice\n");}
<Cod>{codice}	{fprintf(yyout, "	->");
	      ECHO;BEGIN(0);
	      yylval.str=strdup(yytext);
	      return CODICE; }
<Cod>[^A-Z0-9]	{BEGIN(0);
	      fprintf(yyout, "Codice non corretto, la riga verrà ignorata\n");
	      ECHO;}

^"Titolo: "	{BEGIN(Tit);fprintf(yyout, "Analizzo un titolo\n");}
<Tit>{titolo}	{fprintf(yyout, "	->");
	    ECHO;
	    BEGIN(0);
	    yylval.str=strdup(yytext);
	    return TITOLO;}
<Tit>\w+	{BEGIN(0);
	    fprintf(yyout, "Titolo non corretto, la riga verrà ignorata\n");
	    ECHO;}

^"Autore: "	{BEGIN(Aut);fprintf(yyout, "Analizzo un autore\n");}
<Aut>{autore}	{fprintf(yyout, "	->");
	    ECHO;
	    BEGIN(0);
	    yylval.str=strdup(yytext);
	    return AUTORE;}
<Aut>[^{autore}]	{BEGIN(0);
		    fprintf(yyout, "Autore non corretto, la riga verrà ignorata\n");
		    ECHO;}

^{codice}	{fprintf(yyout, "Analizzo un codice	->%s\n",yytext);
	    BEGIN(Ope);
	    yylval.str=strdup(yytext);
	    return CODICE;}
<Ope>" -> "	{return SEPARATORE;}
<Ope>{numero}/(" ")+	{fprintf(yyout, "Analizzo un numero	->%s\n",yytext);
		      yylval.num=atoi(yytext);
		      return NUMERO;}
<Ope>{titolo}/(" ")+	{fprintf(yyout, "Analizzo un nome	->%s\n",yytext);
		    yylval.str=strdup(yytext);
		    return NOME;}
<Ope>";"	{BEGIN(0);return DELIMITATOREOPERAZIONE;}

[\n\r]	{BEGIN(0);linee=yylineno;fprintf(yyout, "\n");}

<<EOF>>  {yyterminate();}
```

---------------


### Sorgente per Bison

---------------


```C
%{
	#include "strutture.h"
	#include <errno.h>
   	void yyerror (const char *s);
   	extern FILE * yyin;
   	Lista *tabella;
   	Lista *e;
   	int completo=TRUE;

%}

%union {
 int num;
 char *str;
}


%token SEPARATORE;
%token DELIMITATORESETTORE;
%token DELIMITATORERECORD;
%token DELIMITATOREOPERAZIONE

%token <str> CODICE;
%token <str> TITOLO;
%token <str> AUTORE;
%token <str> NOME;
%token <num> NUMERO;


%locations
%start Assioma
%error-verbose
%%

Assioma: Elenco_libri DELIMITATORESETTORE Elenco_operazioni
	| Elenco_libri DELIMITATORESETTORE
	{printf("Il file non contiene operazioni");completo=FALSE;}
	| Elenco_libri 
	{printf("Il file non contiene operazioni");completo=FALSE;}
;

Elenco_libri: Elenco_libri DELIMITATORERECORD Libro 
	     | Libro 
;

Libro: CODICE TITOLO AUTORE
      {
		e = (Lista*) malloc(sizeof(Lista));		
		e->codice = $1;
		e->titolo = $2;
		e->autore = $3;
		e->richieste=0;
		e->prev=NULL;
		e->next=NULL;
		hash_insert(e, tabella);
    }
;

Elenco_operazioni: Elenco_operazioni Operazione 
	     | Operazione
;

Operazione: CODICE SEPARATORE NUMERO NOME DELIMITATOREOPERAZIONE
	    {richiesta($1,$3,$4,tabella); free($1); free($4);}
;    
%%

int richiesta(char* codice, int numero,char* nome, Lista* vettore){
  Lista *elemento=hash_get(codice, vettore);
  if(elemento==NULL){
    yyerror("Il testo non e' presente in archivio (errore semantico)");
    return FALSE;
  };
  elemento->richieste+=numero;
  printf("Richiesta registrata\n");
  return TRUE;  
}

Lista* piuvenduto(Lista* vettore){
  int record=0,i;
  Lista *e;
  Lista *iteratore;
  for (i = 0; i < DIMENSIONE; i++) {
	  iteratore=&vettore[i];
	  if(iteratore==NULL)return NULL;
	  iteratore=iteratore->next;
	  while (iteratore != NULL ) {
		  if(iteratore->richieste > record){
		    record=iteratore->richieste;
		    e=iteratore;
		  }
		  iteratore = (Lista*) iteratore->next;
	  }
  }  
  return e;
}

void decalloca(){
  hash_free(tabella);
}


int main(int argc, char *argv[])
{
	int ris;
	char output[256];
	FILE *foutput;
	puts(argv[0]);
	if(argc<2){
	  printf("Non e' stato specificato nessun file in input, verrà usato lo stdin\n");
	}else{
	  strcpy(output,argv[1]);	  
	  strcat(output, "_out.txt");
	  printf("Userò <%s> come file di input e <%s> come file di output\n",argv[1],output);
	  yyin=fopen(argv[1], "r");
	  if(yyin==NULL){
	    printf("Impossibile accedere al file di input\n (%s)\n",strerror(errno));
	    return FALSE;
	  }
	}
	tabella=hash_init();
	ris=yyparse();
	printf("\nStampo un dump dell'hashmap\n");
	puts("-----------------------------------");
	hash_dump(tabella);
	puts("-----------------------------------");
	if(ris!=0){
	  printf("Si sono verificati degli errori,"
		  "il calcolo delle copie non puo' essere effettuato.");
	  return FALSE;
	}
	if(!completo){
	  printf("Il file non conteneva richieste," 
		  "non c'e' niente da contare\n");
	  return TRUE;
	}
	printf("Caricamento completato \n" 
	  "Produco il file di output col nome del libro piu' richiesto.\n");
	Lista *e=piuvenduto(tabella);
	elemento_dump(e);
	if(argc<2){
	    foutput=stdout;
	  }else{
	    foutput=fopen(output,"w+");
	  }
	if(foutput==NULL){
	  printf("Impossibile accedere al file di output\n (%s)\n",strerror(errno));
	  return FALSE;
	}else{
	  fprintf(foutput,"%s\n%d\n",e->titolo,e->richieste);
	  fclose(foutput);
	  printf("Scrittura completata correttamente.\n");
	}	
	decalloca();
	pause();
	return TRUE;
}


void yyerror (const char *s){
	printf("\nErrore di sintassi:\n\t %s \n\tAlla linea %d \n", s, linee);
}

```

---------------


### Sorgente per le funzioni e strutture dati ausiliarie

#### strutture.h

---------------

```C
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

#ifndef STRUTTURE_H
#define STRUTTURE_H

typedef struct _lista{
    char *codice;
    char *titolo;
    char *autore;
    int richieste;
    struct _lista *prev;
    struct _lista *next;
}Lista;

int linee;

//
#define DIMENSIONE 3 // per fare test di carico 
#define TRUE 1
#define FALSE 0
//#define DIMENSIONE 193 // per la release, un numero primo sufficentemente grande.

void elemento_dump(Lista *l);
void lista_dump(Lista *testa);
void hash_dump(Lista *l);
int hash(char* etichetta);
void hash_insert(Lista *dato, Lista vettore[]);
Lista* hash_get(char* codice, Lista vettore[]);
int hash_remove(char* codice, Lista vettore[]);
Lista* hash_init();
int hash_free(Lista *l);
int pause();
#endif
```

#### strutture.c

---------------

```C

#include "strutture.h"

void elemento_dump(Lista *l) {
	if (l == NULL ) {
		printf("NULL\n");
	} else {
		printf("%s:{ %s ; %s ; %d }  [%p <- %p -> %p] \n", l->codice, l->titolo,
				l->autore, l->richieste , l->prev, l, l->next);
	}
}

void lista_dump(Lista *testa) {
	Lista *l = (Lista*) testa->next;
	while (l != NULL ) {
		elemento_dump(l);
		l = (Lista*) l->next;
	}
}

int hash(char* etichetta) {
	int n = 0, key = 0;
	static const int base=127;
	while (etichetta[n] != 0) {
		key = (base*key + etichetta[n]) % DIMENSIONE;
		n++;
	}
	return key;
}


/**
 * Accoda l'elemento nella lista di trabocco corrispondente all'hash
 */
void hash_insert(Lista *dato, Lista vettore[]) {
	if(dato==NULL || vettore==NULL){
		puts("Parametri non validi");
		return;
	}
	int k = hash(dato->codice);
	Lista *temp = &(vettore[k]);
	if(temp==NULL)return;
	while (temp->next != NULL )
		temp = temp->next;
	temp->next = dato;
	dato->prev = temp;
}

Lista* hash_get(char* codice, Lista vettore[]) {
	int k = hash(codice);
	Lista *temp = vettore[k].next; //la testa non si tocca
	if(temp==NULL){
		return NULL;
	};
	while (strcmp(temp->codice, codice) != 0) {
		if (temp->next == NULL )
			return NULL ;
		temp = temp->next;
	}
	return temp;
}
;

int hash_remove(char* codice, Lista vettore[]) {
	Lista *dato = hash_get(codice, vettore);
	Lista *tmp;
	if (dato == NULL )
		return -1;
	if (dato->prev == NULL && dato->next == NULL ) {
		printf("Questo non dovrebbe assolutamente succedere");
		return -1;
	};
	if (dato->prev != NULL ) {
		tmp = dato->prev;
		tmp->next = dato->next;
	};
	if (dato->next != NULL ) {
		tmp = dato->next;
		tmp->prev = dato->prev;
	};
	free(dato->codice);
	free(dato->titolo);
	free(dato->autore);
	free(dato);
	return 1;
}
;

Lista* hash_init() {
	Lista *l;
	int i;
	l = (Lista*) malloc(DIMENSIONE * sizeof(Lista));
	for (i = 0; i < DIMENSIONE; i++) {
		l[i].autore = NULL;
		l[i].codice = NULL;
		l[i].titolo = NULL;
		l[i].richieste=0;
		l[i].next = NULL;
		l[i].prev = NULL;
	}
	return l;
}

void hash_dump(Lista *l) {
	int i;
	for (i = 0; i < DIMENSIONE; i++) {
		printf("[%d]\n", i);
		lista_dump(&(l[i]));
	}
}

int hash_free(Lista *l) {
	int i;
	int n = 0;
	Lista *tmp;
	for (i = 0; i < DIMENSIONE; i++) {
		tmp = l[i].next;//salta la testa.
		while (tmp->next != NULL ) {
			tmp = tmp->next;
			free(tmp->prev->codice);
			free(tmp->prev->titolo);
			free(tmp->prev->autore);
			free(tmp->prev);
			n++;
		}
		free(tmp->codice);
		free(tmp->titolo);
		free(tmp->autore);
		free(tmp);
		l[i].next = NULL;
	}
	return n;
}

/**
 * modo crossplatform per aspettare la pressione di un tasto
 */
int pause(){
    int i;

    for (i = 0; i < 4; i++) {
        printf("Premi INVIO per continuare: ... ");
        if( getchar() == '\n')return 0;
    }    
    return 1;
}

```



## Files allegati

build.sh
:	Script per buildare con gcc, produce un eseguibile con i simboli di debug attivati.

difettoso.txt
:	File di prova che non contiene libri ma contiene operazioni, produce errori

erroresemantico.txt
:	File di prova con una sintassi corretta ma che contiene un errore semantico.

lexer.fl
:	File del Lexer

lex.yy.c
:	File prodotto dal Lexer

makefile
:	Makefile per buildare da sistemi Posix

parser.tab.c
:	File generato da bison ( contiene il main del progetto)

parser.tab.h
:	Header generato da bison

parser.y
:	File dell'analizzatore sintattico

strano.txt
:	File di prova che contiene un elenco di libri e nessuna operazione, non produce errori

strutture.c
:	Implementazione delle strutture dati

strutture.h
:	Header per le strutture dati utilizzate e per la definizione delle costanti

Tesina.bin
:	Binario per Linux (ELF 64bit)

Tesina.dev
:	File di progetto di dev-cpp

Tesina.exe
:	Binario per windows (prodotto con mingw da devcpp)

tesina.md
:	Il sorgente di questo documento

valido.txt
:	File di prova completo in tutte le sue parti


Vincenzo La Spesa

0454965

25/02/2014
